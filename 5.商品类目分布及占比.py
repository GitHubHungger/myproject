"""
1.信誉等级分布及占比
2.好评率分布（小于0.97，大于0.97小于0.98，大于0.98）
3.发货地址分布
4.注册地分布
5.商品类目分布及占比（只统计食品保健、家居用品、美容护理、珠宝配饰、生活服务）
6.服务分析：（取值为商品描述分+店铺服务分+物流服务分的平均值，只统计4.7，4.8，4.9，5.0 结果保留一位小数）
"""
import matplotlib.pyplot as plt
import pandas as pd
df=pd.read_excel('新疆店铺信息采集-皇冠-20201201.xlsx')
x=['食品保健','家居用品','美容护理','珠宝配饰','生活服务']
y=[len(df[df['主营业务']=='食品/保健']),len(df[df['主营业务']=='家居用品']),len(df[df['主营业务']=='美容护理']),len(df[df['主营业务']=='珠宝/配饰']),len(df[df['主营业务']=='生活服务'])]
print(x,y)
plt.rcParams['font.family'] = 'SimHei'
plt.rcParams['axes.unicode_minus']=False
plt.figure(figsize=(8,6))
plt.title('商品类目分布及占比')
plt.pie(y,labels=x,autopct='%.1f%%')
plt.savefig('商品类目分布及占比.png')
plt.show()