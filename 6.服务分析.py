"""
1.信誉等级分布及占比
2.好评率分布（小于0.97，大于0.97小于0.98，大于0.98）
3.发货地址分布
4.注册地分布
5.商品类目分布及占比（只统计食品保健、家居用品、美容护理、珠宝配饰、生活服务）
6.服务分析：（取值为商品描述分+店铺服务分+物流服务分的平均值，只统计4.7，4.8，4.9，5.0 结果保留一位小数）
"""
import matplotlib.pyplot as plt
import pandas as pd
df=pd.read_excel('新疆店铺信息采集-皇冠-20201201.xlsx')
df=(df['商品描述分']+df['店铺服务分']+df['物流服务分'])/3
pf=[ float(f'{i:.1f}') for i in list(df)]
f47=0
x=['4.7','4.8','4.9','5.0']
y=[0,0,0,0]
for i in  pf:
    if i==4.7:
        y[0]+=1
    elif i==4.8:
        y[1] += 1
    elif i==4.9:
        y[2] += 1
    elif i==5.0:
        y[3] += 1
print(x,y)
plt.rcParams['font.family'] = 'SimHei'
plt.rcParams['axes.unicode_minus']=False
plt.figure()
plt.title('服务分析')
plt.bar(x,y)
plt.savefig('服务分析.png')
plt.show()